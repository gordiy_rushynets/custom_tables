// Tables.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <typename T, typename T2>
class Table {

private:
	// create set of pairs with key and value
	pair<T, T2>* table;
	int size;

public:
	Table()
	{
		size = 1;
		table = new pair<T, T2>[size];
	}

	~Table()
	{
		delete[] table;
	}

	void resize_array()
	{
		// create temporary table
		pair<T, T2>* exange_table = new pair<T, T2>[size];
		// copy elements from table in exchange_table
		for (int i = 0; i < size; i++)
		{
			exange_table[i] = table[i];
		}
		// remove previous table
		delete[] table;

		size += 1;

		// initialize previous table with new size
		table = new pair<T, T2>[size];

		for (int i = 0; i < size - 1; i++)
		{
			table[i] = exange_table[i];
		}
	}

	void Add_Element(T k, T2 v)
	{
		resize_array();
		// intialize temorary pair
		pair<T, T2> x;

		// add key
		x.first = k;
		// add value
		x.second = v;
		// set element x in table
		table[size - 1] = x;
	}

	// get element of pair
	pair<T, T2> GetPair(int i)
	{
		return table[i];
	}

	// search element by key
	T2 Search_Element(T k)
	{
		for (int i = 0; i < size; i++)
		{
			if (table[i].first == k)
			{
				// return value
				return table[i].second;
			}
		}
		cout << "It's wrong key in table:" << endl;
		return 0;
	}
	// remove element by key
	void Delete_Element(T k)
	{
		for (int i = 0; i < size; i++) {
			if (table[i].first == k)
			{
				for (int j = i; j < size - 1; j++)
				{
					table[j] = table[j + 1];
				}
			}
		}
	}
	// show table
	void Show()
	{
		for (int i = 1; i < size; i++) {
			cout << table[i].first << " " << table[i].second << endl;
		}
	}

	void Replece_Value(T k, T2 v)
	{
		for (int i = 0; i < size; i++)
		{
			if (table[i].first == k)
			{
				table[i].second = v;
				return;
			}
		}
		cout << "Error" << endl;
	}
	
	int get_size()
	{
		return size;
	}
};

int main()
{
	
	system("pause");
	return 0;
}
