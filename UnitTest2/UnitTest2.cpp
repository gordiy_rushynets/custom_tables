#include "pch.h"
#include "CppUnitTest.h"
#include <string>
#include "../HashNode/HashMap.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace UnitTest2
{
	TEST_CLASS(UnitTest2)
	{
	public:
		
		TEST_METHOD(CheckingGET)
		{
			HashMap<int, string, 10, MyKeyHash> hmap;
			hmap.put(1, "1");
			hmap.put(2, "2");
			hmap.put(3, "3");

			string value;
			bool result = hmap.get(2, value);

			Assert::IsTrue(result);
		}

		TEST_METHOD(CheckingRemove)
		{
			HashMap<int, string, 10, MyKeyHash> hmap;
			hmap.put(1, "1");
			hmap.put(2, "2");
			hmap.put(3, "3");

			string value;
			bool result;

			hmap.remove(3);
			result = hmap.get(3, value);
			Assert::IsTrue(!result);
		}
	};
}
