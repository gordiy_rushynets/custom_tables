#include "pch.h"
#include "CppUnitTest.h"
#include "Tables.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(CheckingResizeArray)
		{
			Table<int, int> t;

			t.Add_Element(1, 1);
			t.Add_Element(2, 2);
			t.Add_Element(3, 3);
			t.Add_Element(4, 4);
			t.Add_Element(5, 5);

			bool condition = true;
			int i = 1;

			for (int i = 0; i < 5; i++)
			{
				if (t.GetPair(i).first == i)
				{
				}
				else
				{
					Assert::IsTrue(false);
				}
			}
		}

		TEST_METHOD(CheckingSearchElement)
		{
			Table<int, float> t;

			t.Add_Element(1, 1);
			t.Add_Element(2, 2);
			t.Add_Element(3, 3);
			t.Add_Element(4, 4);
			t.Add_Element(5, 5);

			bool condition = true;
			int i = 1;

			for (int i = 1; i <= 5; i++)
			{
				if (!(t.Search_Element(i) == i))
				{
					Assert::IsTrue(false);
				}
				else
				{
					Assert::IsTrue(true);
				}
			}
		}
	};
}
